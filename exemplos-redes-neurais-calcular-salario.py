import numpy as np
import pandas as pd
import plotly.express as px

from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures, StandardScaler, LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor

# Carregar os dados do CSV
df = pd.read_csv("job_level.csv")

X = df.drop(columns=["salario"], axis=1)
y = df["salario"]

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

# Normalizar os dados
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)


# Modelo de RandomForestRegressor
# model = RandomForestRegressor(n_estimators=50, random_state=42)
# model.fit(X_train_scaled, y_train)
# y_pred_rf = model.predict(X_test_scaled)
# r2_rf = r2_score(y_test, y_pred_rf)
# print(f'Random Forest R^2: {r2_rf}')

model = GradientBoostingRegressor(n_estimators=150, learning_rate=0.1, max_depth=4, random_state=42)
model.fit(X_train_scaled, y_train)
y_pred = model.predict(X_test_scaled)
r2 = r2_score(y_test, y_pred)
print(f'Gradient Boosting R^2: {r2}')

# Modelo de Regressão Linear Basico
# model = LinearRegression()
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f"Linear Regression R^2: {r2}")


# Pipeline de Regressão Polinomial
# model = Pipeline([
#     ('poly', PolynomialFeatures(degree=2)),
#     ('linear', LinearRegression())
# ])
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f'Polynomial Regression R^2: {r2}')

# Modelo de Ridge Regression
# model = Ridge(alpha=1.0)
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f'Ridge Regression R^2: {r2}')

# Modelo de Lasso Regression
# model = Lasso(alpha=0.1)
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f'Lasso Regression R^2: {r2}')


# Inicializar e treinar o classificador MLP
# model = MLPClassifier(
#     activation="relu",
#     solver="adam",
#     random_state=42,
#     alpha=0.01,
#     max_iter=150,
#     learning_rate_init=0.01
# )
# model.fit(X_train_scaled, y_train)
# y_pred = model.predict(X_test_scaled)
# r2 = r2_score(y_test, y_pred)
# print(f'Redes Neurais R^2: {r2}')

# Testar nova instância
nova_instancia = pd.DataFrame(
    {
        "tempo_experiencia": [12],
        "faculdade_concluida_0_1": [1],
        "pos_graduado_0_1": [0],
        "conhecimento_programacao_0_10": [10],
        "conheciemento_das_ferramentas_trabalho_0_10": [10],
        "conhecimento_banco_de_dados_0_10": [10],
        "conhecimento_back_end_0_10": [10],
        "conhecimento_front_end_0_10": [10],
        "conhecimento_devops_0_10": [10],
        "communicacao_0_10": [10],
        "dominio_sistemas_operacionais_0_5": [5],
        "conhecimento_basico_rede_0_5": [5],
        "lideranca_0_10": [10],
    }
)

nova_instancia_scaled = scaler.transform(nova_instancia)

# # Fazer a previsão
previsao = model.predict(nova_instancia_scaled)
print(f"A previsão para a nova instância é: {previsao[0]}")


# Função para criar o gráfico de dispersão com Plotly
def plot_real_vs_pred(y_test, y_pred, title):
    fig = px.scatter(
        x=y_test,
        y=y_pred,
        labels={"x": "Valores Reais", "y": "Valores Preditos"},
        title=title,
    )
    fig.add_shape(
        type="line",
        x0=min(y_test),
        y0=min(y_test),
        x1=max(y_test),
        y1=max(y_test),
        line=dict(color="Red", dash="dash"),
    )
    fig.show()

# Criar o gráfico para o modelo de Regressão Linear
# plot_real_vs_pred(y_test, y_pred, 'Regressão Linear: Valores Reais vs. Valores Preditos')

